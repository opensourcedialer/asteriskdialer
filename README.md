# AsteriskDialer

Open Source Asterisk Dialer

monolithic dialer features.

- blacklist a single number and list of numbers to upload. This works as local DND { do not disturb }
  * have option to search number
  * enable and disable DND option
- lead/data upload and auto dialing. Predictive Dialer feature.
- attendant tranfer, I think will see this later
- call back from already dialed number. will see later.
- call barge
- live monitoring
- call transfer
- call recording , in.ula and out.ula  ? or direct mp3  ?
- sip to sip call feature ?  option to enable and disable feature. ?
- cdr records
  * search call records against a number
  * filter option by date wise
  * filter option disposition wise
  * filter based on hangup cause from Provider like cause 21, 31 etc.  
    https://wiki.asterisk.org/wiki/display/AST/Hangup+Cause+Mappings
  * filter based on answered, busy, ringing, unreachable etc.
- voicemail ? I think not required for now.
- IVR
  * Customer can generate IVR by themself ?
  * upload recorded file
  * transfer in queue or on a mobile number if press zero or desired key
- queue
  * transfer in queue
  * music on hold
  * music in queue untill customer pick the call
  * how long the customre will be in queue ?
- incoming  
  - transfer on extension or number
- disposition report
- default crm provided by us
  * Box integeration with client CRM if they provide.
- twilio integeration, for pilvo or zentrunk will see later.

SIP authentication and add on's
 - sip enable/diable. Option to map the SIP for host IP address only ? So sip will register on mentioned IP address only.
 - select provider if have multiple provider. Set default provider ?
 - select DID before dial number
 - prefix for the campaign wise. Also have option to set default prefix for all campaign.

Box Admin Web Login
 - restart asterisk
 - restart tomcat
 - Box configuration like cpu, ram , disk space etc 
